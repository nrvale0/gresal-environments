filebucket { 'main': server => 'master', path   => false, }
File { backup => 'main' }

node /^master.*$/ {
  include ::site::profile::pe::master
  hiera_include('classes', '::site::profile::pe::agent')
}

node /^ubuntu0.*$/ {
  # this is supposed to fail!
  exec { '/bin/shizzle': }
}
node default { hiera_include('classes', '::site::profile::pe::agent') }
